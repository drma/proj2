package com.example.proj2;

import androidx.annotation.NonNull;

import com.google.firebase.database.Exclude;

public class AppUser {

    @Exclude
    private String id;
    private String name;

    public AppUser() {
    }

    public AppUser(String name) {
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return "User{ name: " + name + " }";
    }
}
