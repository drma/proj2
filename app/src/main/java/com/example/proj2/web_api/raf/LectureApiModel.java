package com.example.proj2.web_api.raf;

import com.example.proj2.main.tabs.schedule.model.Lecture;
import com.google.gson.annotations.SerializedName;

public class LectureApiModel {

    @SerializedName("predmet")
    private String subject;

    @SerializedName("tip")
    private String lectureType;

    @SerializedName("nastavnik")
    private String professor;

    @SerializedName("grupe")
    private String groups;

    @SerializedName("dan")
    private String day;

    @SerializedName("termin")
    private String time;

    @SerializedName("ucionica")
    private String classroom;

    public String getSubject() {
        return subject;
    }

    public String getLectureType() {
        return lectureType;
    }

    public String getProfessor() {
        return professor;
    }

    public String getGroups() {
        return groups;
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }

    public String getClassroom() {
        return classroom;
    }

    public static Lecture toEntity(LectureApiModel l) {
        return new Lecture(
                l.getSubject(),
                l.getLectureType()
        );
    }
}
