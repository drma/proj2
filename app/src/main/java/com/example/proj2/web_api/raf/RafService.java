package com.example.proj2.web_api.raf;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RafService {

    @GET("raspored/json.php")
    public Call<List<LectureApiModel>> getAllLectures();
}
