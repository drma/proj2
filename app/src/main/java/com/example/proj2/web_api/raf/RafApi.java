package com.example.proj2.web_api.raf;

import java.util.List;

import retrofit2.Call;

public class RafApi {

    private RafService rafService;

    public RafApi() {
        rafService = RafServiceGenerator.createService(RafService.class);
    }

    public Call<List<LectureApiModel>> getLectures() {
        return rafService.getAllLectures();
    }
}
