package com.example.proj2;

import androidx.room.TypeConverter;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Date;

public class RoomTypeConverters {

    @TypeConverter
    public static LocalTime fromTimestamp(String value) {
        if (value == null) {
            return null;
        }

        String[] timestamp = value.split(":");
        String hour = timestamp[0];
        String minute = timestamp[1];
        return LocalTime.of(
                Integer.parseInt(hour),
                Integer.parseInt(minute)
        );
    }

    @TypeConverter
    public static String ToTimestamp(LocalTime localTime) {
        if (localTime == null) {
            return null;
        }

        return localTime.getHour() + ":" + localTime.getMinute();
    }

    @TypeConverter
    public static DayOfWeek fromIndex(Integer value) {
        return DayOfWeek.of(value);
    }

    @TypeConverter
    public static Integer toIndex(DayOfWeek value) {
        return value.getValue();
    }
}
