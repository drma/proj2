package com.example.proj2.util;

import com.example.proj2.AppUser;

@FunctionalInterface
public interface OnUserClick {

    public void onUserClick(AppUser user);
}
