package com.example.proj2.util;

import androidx.annotation.NonNull;

import com.example.proj2.AppUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UsersValueChangeListener implements ValueEventListener {
    @FunctionalInterface
    public static interface OnValue {
        public void onValueEvent(List<AppUser> users);
    }


    private OnValue cb;

    public UsersValueChangeListener(OnValue cb) {
        this.cb = cb;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        if (dataSnapshot.getValue() == null) {
            return;
        } else {
            List<AppUser> users = new ArrayList<>();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                users.add(snapshot.getValue(AppUser.class));
            }
            this.cb.onValueEvent(users);
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
