package com.example.proj2.vm;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.proj2.AppUser;
import com.example.proj2.util.SharedPreferenceKeys;

import java.util.List;

public class AppViewModel extends AndroidViewModel {

    private AuthLiveData authLiveData;
    private UsersLiveData usersLiveData;
    private MutableLiveData<String> signedInUsernameLiveData;
    private LiveData<AppUser> signedInUserLiveData;
    private SharedPreferences sharedPreferences;

    public AppViewModel(@NonNull Application application) {
        super(application);

        authLiveData = new AuthLiveData();
        usersLiveData = new UsersLiveData();
        signedInUsernameLiveData = new MutableLiveData<>();

        sharedPreferences = application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
        signedInUsernameLiveData.setValue(sharedPreferences.getString(SharedPreferenceKeys.USERNAME, null));
        sharedPreferences.registerOnSharedPreferenceChangeListener(this::onSignedInUsernameChange);

        signedInUserLiveData = Transformations.map(signedInUsernameLiveData, username -> {
            List<AppUser> users = usersLiveData.getValue();
           AppUser user = users.stream()
                   .filter(u -> u.getName().equals(username))
                   .findFirst().get();
           return user;
        });

    }

    private void onSignedInUsernameChange(SharedPreferences sharedPreferences, String s) {
        if (s.equals(SharedPreferenceKeys.USERNAME)) {
            signedInUsernameLiveData.setValue(sharedPreferences.getString(s, ""));
        }
    }

    public AuthLiveData getAuthLiveData() {
        return authLiveData;
    }

    public UsersLiveData getUsersLiveData() {
        return usersLiveData;
    }

    public LiveData<AppUser> getSignedInUserLiveData() {
        return signedInUserLiveData;
    }

    public void logout() {
        sharedPreferences
                .edit()
                .clear()
                .apply();
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getApplication().startActivity(intent);
    }

}
