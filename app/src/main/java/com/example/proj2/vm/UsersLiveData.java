package com.example.proj2.vm;

import android.util.Log;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.proj2.AppUser;
import com.example.proj2.util.UsersValueChangeListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UsersLiveData extends LiveData<List<AppUser>> {
    private static final String TAG = "USERS_LIVE_DATA";

    private ValueEventListener eventListener;
    private DatabaseReference usersReference;

    public UsersLiveData() {
        super();

        usersReference = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("users");

        eventListener = new UsersValueChangeListener(this::setValue);

        usersReference.addValueEventListener(eventListener);
    }

//    @Override
//    protected void onActive() {
//        super.onActive();
//
//        usersReference.addValueEventListener(eventListener);
//    }
//
//    @Override
//    protected void onInactive() {
//        super.onInactive();
//
//        usersReference.removeEventListener(eventListener);
//    }
}
