package com.example.proj2.vm;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AuthLiveData extends LiveData<FirebaseUser> {
    private static final String TAG = "AUTH_LIVE_DATA";

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener listener;

    public AuthLiveData() {
        super();

        firebaseAuth = FirebaseAuth.getInstance();
        Log.d(TAG, firebaseAuth.getFirebaseAuthSettings().toString());
        listener = this::onAuthStateChange;
    }

    private void onAuthStateChange(FirebaseAuth firebaseAuth) {
        this.setValue(firebaseAuth.getCurrentUser());
    }

    @Override
    protected void onActive() {
        super.onActive();

        firebaseAuth.addAuthStateListener(listener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        firebaseAuth.removeAuthStateListener(listener);
    }
}
