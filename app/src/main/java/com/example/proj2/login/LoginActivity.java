package com.example.proj2.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.example.proj2.MainActivity;
import com.example.proj2.R;
import com.example.proj2.databinding.ActivityLoginBinding;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGIN_ACTIVITY";

    private static final String THE_PASSWORD = "password";
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        binding.setOnLoginButtonClick(this::handleLoginButtonClick);
    }

    private void handleLoginButtonClick(View v) {
        String username = binding.getUsername();
        String password = binding.getPassword();

        if (password.equals(THE_PASSWORD)) {
            showToast("Login success!");
            attemptLogin(username, password);
        } else {
            showToast("Bad password");
        }
    }

    private void attemptLogin(String username, String password) {
        new Handler().post(() -> {
            Task<Void> login = ViewModelProviders.of(this)
                    .get(LoginViewModel.class)
                    .login(username, password);

            if (login == null) {
                transferToMainActivity();
            } else {
                login.addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        transferToMainActivity();
                    }
                });
            }

        });
    }

    private void transferToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void showToast(String text) {
        Toast
                .makeText(this, text, Toast.LENGTH_SHORT)
                .show();
    }
}
