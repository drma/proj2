package com.example.proj2.login;

@FunctionalInterface
public interface LoginClickListener {

    public void onLoginClick(String username, String password);
}
