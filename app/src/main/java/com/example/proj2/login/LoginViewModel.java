package com.example.proj2.login;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.proj2.AppUser;
import com.example.proj2.util.SharedPreferenceKeys;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginViewModel extends AndroidViewModel {

    private SharedPreferences sharedPreferences;
    private DatabaseReference usersReference;

    public LoginViewModel(@NonNull Application application) {
        super(application);

        sharedPreferences = application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
        usersReference = FirebaseDatabase.getInstance().getReference().child("users");
    }

    public Task<Void> login(String username, String password) {
        sharedPreferences
                .edit()
                .putString(SharedPreferenceKeys.USERNAME, username)
                .apply();

        // TODO spreci duplikate
        boolean userExists = false;
        if (!userExists) {
            return usersReference
                    .child(username)
                    .setValue(new AppUser(username));
        } else {
            return null;
        }
    }
}

//class LoginResponse {
//    private AppUser user;
//    private boolean success;
//
//    public LoginResponse(AppUser user, boolean success) {
//        this.user = user;
//        this.success = success;
//    }
//
//    public AppUser getUser() {
//        return user;
//    }
//
//    public boolean isSuccess() {
//        return success;
//    }
//}
