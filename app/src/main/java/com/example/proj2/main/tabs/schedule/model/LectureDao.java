package com.example.proj2.main.tabs.schedule.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LectureDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Lecture lecture);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Lecture> lectures);

    @Query("SELECT * FROM raf_lecture")
    LiveData<List<Lecture>> getAllLectures();

    @Query("DELETE FROM raf_lecture")
    void deleteAllLectures();
}
