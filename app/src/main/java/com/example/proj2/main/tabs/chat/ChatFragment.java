package com.example.proj2.main.tabs.chat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.proj2.AppUser;
import com.example.proj2.R;
import com.example.proj2.databinding.ChatFragmentBinding;
import com.example.proj2.databinding.ContactCardBinding;
import com.example.proj2.main.tabs.chat.rv.contact.ContactRVAdapter;
import com.example.proj2.vm.AppViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ChatFragment extends Fragment {
    private static final String TAG = "CHAT_FRAGMENT";

    public static ChatFragment newInstance() {
        ChatFragment instance = new ChatFragment();
        Bundle args = new Bundle();
        instance.setArguments(args);
        return instance;
    }

    private AppViewModel vm;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ChatFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.chat_fragment, container, false);
        binding.setLifecycleOwner(this);

        ContactRVAdapter adapter = new ContactRVAdapter(this::onUserClick);
        binding.setRvAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.setRvLayoutManager(layoutManager);

        vm = ViewModelProviders.of(this).get(AppViewModel.class);
        vm.getUsersLiveData().observe(this, adapter::setDataset);

        return binding.getRoot();
    }

    private void onUserClick(AppUser appUser) {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra(ChatActivity.EXTRA_OTHER_USER, appUser.getName());
        startActivity(intent);
    }
}
