package com.example.proj2.main.tabs.schedule.rv;

import androidx.recyclerview.widget.DiffUtil;

import com.example.proj2.main.tabs.schedule.model.Lecture;

import java.util.List;

public class ScheduleRVDiffCallback extends DiffUtil.Callback {

    private List<Lecture> oldList;
    private List<Lecture> newList;

    public ScheduleRVDiffCallback(List<Lecture> oldList, List<Lecture> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }


    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Lecture oldItem = oldList.get(oldItemPosition);
        Lecture newItem = newList.get(newItemPosition);
        return oldItem.equals(newItem);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
