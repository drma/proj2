package com.example.proj2.main.tabs.chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.proj2.AppUser;
import com.example.proj2.R;
import com.example.proj2.databinding.ActivityChatBinding;
import com.example.proj2.main.tabs.chat.model.Message;
import com.example.proj2.main.tabs.chat.rv.messages.MessageRVAdapter;
import com.example.proj2.util.SharedPreferenceKeys;
import com.example.proj2.vm.AppViewModel;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ChatActivity extends AppCompatActivity {
    private static final String TAG = "CHAT_ACTIVITY";

    public static final String EXTRA_OTHER_USER = "OTHER_USER";

    private ActivityChatBinding binding;
    private AppViewModel appVm;
    private ChatViewModel chatVm;
    private String otherUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parseIntent();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        binding.setLifecycleOwner(this);
        MessageRVAdapter adapter = new MessageRVAdapter(otherUser);
        binding.setRvAdapter(adapter);
        binding.setOnSendClickListener(this::onSendClick);

        ViewModelProvider provider = ViewModelProviders.of(this);
        appVm = provider.get(AppViewModel.class);
        chatVm = provider.get(ChatViewModel.class);

        chatVm.getMessagesLiveData().observe(this, messages -> {
            String currentUser = fakeFrom().getName();
            List<Message> messagesInChat = messages.stream()
                    .filter(m ->
                            (m.getFrom().getName().equals(currentUser) && m.getTo().getName().equals(otherUser))
                                    ||
                                    (m.getFrom().getName().equals(otherUser) && m.getTo().getName().equals(currentUser))
                    )
                    .collect(Collectors.toList());
            adapter.setMessages(messagesInChat);
        });
    }

    private void onSendClick(View view) {
        String content = binding.getMessage();
//        AppUser from = appVm.getSignedInUserLiveData().getValue();
        AppUser from = fakeFrom();
        AppUser to = appVm.getUsersLiveData().getValue().stream()
                .filter(u -> u.getName().equals(otherUser))
                .findFirst().get();

        Message message = new Message(null, from, to, content, new Date());
        // hol up
        Log.d(TAG, message.getTimestamp().toGMTString());

        chatVm.sendMessage(message);
    }

    private void parseIntent() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        otherUser = intent.getStringExtra(EXTRA_OTHER_USER);
    }

    private AppUser fakeFrom() {
        String username = getSharedPreferences(getPackageName(), MODE_PRIVATE).getString(SharedPreferenceKeys.USERNAME, "");
        return appVm.getUsersLiveData().getValue().stream()
                .filter(u -> u.getName().equals(username))
                .findFirst().get();
    }
}
