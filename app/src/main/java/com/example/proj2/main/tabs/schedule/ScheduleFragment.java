package com.example.proj2.main.tabs.schedule;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.R;
import com.example.proj2.databinding.ScheduleBinding;
import com.example.proj2.main.tabs.schedule.model.Lecture;
import com.example.proj2.main.tabs.schedule.rv.ScheduleRVAdapter;

import java.lang.reflect.Array;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class ScheduleFragment extends Fragment {
    private static final String TAG = "SCHEDULE_FRAGMENT";

    public static ScheduleFragment newInstance() {
        ScheduleFragment instance = new ScheduleFragment();
        Bundle args = new Bundle();
        instance.setArguments(args);
        return instance;
    }

    private ScheduleBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.schedule, container, false);
        binding.setLifecycleOwner(this);

        ScheduleViewModel vm = ViewModelProviders.of(getActivity()).get(ScheduleViewModel.class);

        ScheduleRVAdapter adapter = new ScheduleRVAdapter();
        binding.setRvAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.setLayoutManager(layoutManager);
        binding.setRvAdapter(adapter);
        binding.executePendingBindings();

        vm.getLectures().observe(this, adapter::setDataset);
        vm.getLectures().observe(this, this::setGroups);
        new Handler().postDelayed(vm::fetchLectures, 100);

        List<DayOfWeek> days = Arrays.asList(DayOfWeek.values());
        ArrayAdapter<String> dayAdapter = new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, days);
        binding.setDaySpinnerAdapter(dayAdapter);
        binding.dayOfWeekSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setDayOfWeekFilter(days.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.setGroupSpinnerAdapter(new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, Collections.emptyList()));
        binding.groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setGroupFilter((String) binding.getGroupSpinnerAdapter().getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return binding.getRoot();
    }

    private void setGroups(List<Lecture> lectures) {
//        List<String> groupsWithDuplicates = lectures.stream()
//                .map(l -> Arrays.asList(l.getGroups().split(", ")))
//                .flatMap(List::stream)
//                .collect(Collectors.toList());
//
//        List<String> groups = new ArrayList<>(new LinkedHashSet<String>(groupsWithDuplicates));
//        binding.setGroupSpinnerAdapter(new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, groups));
    }


}
