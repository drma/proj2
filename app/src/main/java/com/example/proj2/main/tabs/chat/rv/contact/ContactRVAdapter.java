package com.example.proj2.main.tabs.chat.rv.contact;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.AppUser;
import com.example.proj2.R;
import com.example.proj2.databinding.ContactCardBinding;
import com.example.proj2.util.DefaultRVDiffCallback;
import com.example.proj2.util.OnUserClick;

import java.util.ArrayList;
import java.util.List;

public class ContactRVAdapter extends RecyclerView.Adapter<ContactViewHolder> {
    private static final String TAG = "CONTACT_RV_ADAPTER";

    private List<AppUser> dataset = new ArrayList<>();

    private OnUserClick onUserClick;

    public ContactRVAdapter(OnUserClick onUserClick) {
        this.onUserClick = onUserClick;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ContactCardBinding binding = DataBindingUtil.inflate(inflater, R.layout.contact_card, parent, false);
        binding.setOnClick(onUserClick);
        return new ContactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        holder.setContact(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setDataset(List<AppUser> newDataset) {
        Log.d(TAG, newDataset.toString());
        DefaultRVDiffCallback<AppUser> cb = new DefaultRVDiffCallback<>(dataset, newDataset);
        DiffUtil.DiffResult diff = DiffUtil.calculateDiff(cb);
        dataset.clear();
        dataset.addAll(newDataset);
        diff.dispatchUpdatesTo(this);
    }
}
