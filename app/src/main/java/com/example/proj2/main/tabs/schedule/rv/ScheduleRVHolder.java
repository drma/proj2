package com.example.proj2.main.tabs.schedule.rv;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.databinding.ScheduleCardBinding;
import com.example.proj2.main.tabs.schedule.model.Lecture;

public class ScheduleRVHolder extends RecyclerView.ViewHolder {

    private ScheduleCardBinding binding;


    public ScheduleRVHolder(@NonNull ScheduleCardBinding binding) {
        super(binding.getRoot());

        this.binding = binding;
    }

    public void setData(Lecture lecture) {
        binding.setLecture(lecture);
    }
}
