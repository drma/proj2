package com.example.proj2.main.tabs.schedule.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Entity(tableName = "raf_lecture")
public class Lecture {
    public static final String TABLE_NAME = "raf_lecture";

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Long id;
    private String subject;
    private String lectureType;
//    private String groups;
//    private DayOfWeek day;

    public Lecture() {

    }


    public Lecture(String subject, String lectureType) {
        this.subject = subject;
        this.lectureType = lectureType;
    }

    @NonNull
    public Long getId() {
        return id;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLectureType() {
        return lectureType;
    }

    public void setLectureType(String lectureType) {
        this.lectureType = lectureType;
    }

//    public String getGroups() {
//        return groups;
//    }
//
//    public void setGroups(String groups) {
//        this.groups = groups;
//    }
//
//    public DayOfWeek getDay() {
//        return day;
//    }
//
//    public void setDay(DayOfWeek day) {
//        this.day = day;
//    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return false;
    }
}
