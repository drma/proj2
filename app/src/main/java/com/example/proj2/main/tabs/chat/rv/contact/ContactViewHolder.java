package com.example.proj2.main.tabs.chat.rv.contact;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.AppUser;
import com.example.proj2.databinding.ContactCardBinding;

public class ContactViewHolder extends RecyclerView.ViewHolder {

    private ContactCardBinding binding;

    public ContactViewHolder(ContactCardBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setContact(AppUser user) {
        binding.setContact(user);
    }
}
