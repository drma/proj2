package com.example.proj2.main.tabs.chat.rv.messages;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.R;
import com.example.proj2.databinding.ChatMessageBinding;
import com.example.proj2.main.tabs.chat.model.Message;
import com.example.proj2.util.DefaultRVDiffCallback;

import java.util.ArrayList;
import java.util.List;

public class MessageRVAdapter extends RecyclerView.Adapter<MessageViewHolder> {
    private static final String TAG = "MESSAGES_RV_ADAPTER";

    private List<Message> messages = new ArrayList<>();
    private String toUsername;

    public MessageRVAdapter(String toUsername) {
        this.toUsername = toUsername;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ChatMessageBinding binding = DataBindingUtil.inflate(inflater, R.layout.chat_message, parent, false);
        return new MessageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        holder.setMessage(messages.get(position));
        holder.setIsReceived(messages.get(position).getFrom().getName().equals(toUsername));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void setMessages(List<Message> messages) {
        DiffUtil.Callback cb = new DefaultRVDiffCallback<Message>(this.messages, messages);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(cb);
        this.messages.clear();
        this.messages.addAll(messages);
        result.dispatchUpdatesTo(this);
    }
}

