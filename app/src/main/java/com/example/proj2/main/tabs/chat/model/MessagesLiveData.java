package com.example.proj2.main.tabs.chat.model;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.proj2.AppUser;
import com.example.proj2.vm.UsersLiveData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MessagesLiveData extends LiveData<List<Message>> {
    private static final String TAG = "MESSAGES_LIVE_DATA";

    private ValueEventListener valueEventListener;
    private DatabaseReference messagesReference;
    private UsersLiveData usersLiveData;

    public MessagesLiveData() {
        super();

        DatabaseReference rootReference = FirebaseDatabase.getInstance().getReference();
        messagesReference = rootReference.child("messages");
        usersLiveData = new UsersLiveData();

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    return;
                } else {
                    List<MessageFirebaseModel> firebaseMessages = new ArrayList<>();
                    for (DataSnapshot s : dataSnapshot.getChildren()) {
                        MessageFirebaseModel firebaseMessage = s.getValue(MessageFirebaseModel.class);
                        firebaseMessage.setId(s.getKey());
                        firebaseMessages.add(firebaseMessage);
                    }

                    List<AppUser> users = usersLiveData.getValue();
                    if (users == null) {
                        return;
                    }
                    List<Message> messages = firebaseMessages.stream()
                            .map(fm -> {
                                AppUser from = users.stream()
                                        .filter(u -> u.getName().equals(fm.getFrom()))
                                        .findFirst().get();
                                AppUser to = users.stream()
                                        .filter(u -> u.getName().equals(fm.getTo()))
                                        .findFirst().get();
                                return new Message(
                                        fm.getId(),
                                        from,
                                        to,
                                        fm.getContent(),
                                        Date.from(Instant.ofEpochSecond(Long.parseLong(fm.getTimestamp())))
                                );
                            })
                            .collect(Collectors.toList());
                    setValue(messages);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        messagesReference.addValueEventListener(valueEventListener);
    }
}
