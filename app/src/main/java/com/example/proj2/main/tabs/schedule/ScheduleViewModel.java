package com.example.proj2.main.tabs.schedule;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.proj2.AppDatabase;
import com.example.proj2.main.tabs.schedule.model.Lecture;
import com.example.proj2.main.tabs.schedule.model.LectureDao;
import com.example.proj2.web_api.raf.LectureApiModel;
import com.example.proj2.web_api.raf.RafApi;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleViewModel extends AndroidViewModel {
    private static final String TAG = "ScheduleViewModel";

    private RafApi rafApi;
    private LectureDao dao;
    private ExecutorService executor;

    public ScheduleViewModel(@NonNull Application application) {
        super(application);
        rafApi = new RafApi();
        dao = AppDatabase.getInstance(application).getLectureDao();
        executor = Executors.newCachedThreadPool();
    }

    public LiveData<List<Lecture>> getLectures() {
        return dao.getAllLectures();
    }

    public void fetchLectures() {
        rafApi.getLectures().enqueue(new Callback<List<LectureApiModel>>() {
            @Override
            public void onResponse(Call<List<LectureApiModel>> call, Response<List<LectureApiModel>> response) {
                Log.d(TAG, "Fetch successfull!");
                insertMovies(response.body());
            }

            @Override
            public void onFailure(Call<List<LectureApiModel>> call, Throwable t) {
                Log.e(TAG, "Lecture fetch failed");
                t.printStackTrace();
            }
        });
    }

    private void insertMovies(List<LectureApiModel> body) {
        executor.submit(() -> {
            this.dao.deleteAllLectures();
            List<Lecture> lectures = body.stream()
                            .map(LectureApiModel::toEntity)
                            .collect(Collectors.toList());
            this.dao.insert(lectures);
        });
    }
}
