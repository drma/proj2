package com.example.proj2.main.tabs.chat.model;

import com.example.proj2.AppUser;
import com.google.firebase.database.Exclude;

import java.util.Date;

public class Message {

    @Exclude
    private String id;

    private AppUser from;
    private AppUser to;
    private String content;
    private Date timestamp;

    public Message(String id, AppUser from, AppUser to, String content, Date timestamp) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.content = content;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AppUser getFrom() {
        return from;
    }

    public AppUser getTo() {
        return to;
    }

    public String getContent() {
        return content;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
