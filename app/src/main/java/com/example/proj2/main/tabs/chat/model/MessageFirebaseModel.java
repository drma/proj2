package com.example.proj2.main.tabs.chat.model;

import com.google.firebase.database.Exclude;

public class MessageFirebaseModel {

    @Exclude
    private String id;
    private String from;
    private String to;
    private String content;
    private String timestamp;

    public MessageFirebaseModel() {

    }

    public MessageFirebaseModel(String id, String from, String to, String content, String timestamp) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.content = content;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getContent() {
        return content;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
