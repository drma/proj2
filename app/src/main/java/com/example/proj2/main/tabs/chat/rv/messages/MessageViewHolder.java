package com.example.proj2.main.tabs.chat.rv.messages;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.databinding.ChatMessageBinding;
import com.example.proj2.main.tabs.chat.model.Message;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    private ChatMessageBinding binding;

    public MessageViewHolder(ChatMessageBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setMessage(Message message) {
        binding.setMessage(message);
    }

    public void setIsReceived(boolean isReceived) {
        binding.setIsReceived(isReceived);
    }
}
