package com.example.proj2.main.tabs.schedule.rv;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proj2.R;
import com.example.proj2.databinding.ScheduleCardBinding;
import com.example.proj2.main.tabs.schedule.model.Lecture;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ScheduleRVAdapter extends RecyclerView.Adapter<ScheduleRVHolder> {

    private List<Lecture> dataset = Arrays.stream(new Lecture[] {
            new Lecture("test", "prvi"),
            new Lecture("test", "drugi")
    }).collect(Collectors.toList());

    private DayOfWeek dayOfWeekFilter = null;
    private String groupFilter = "";

    @NonNull
    @Override
    public ScheduleRVHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ScheduleCardBinding binding = DataBindingUtil.inflate(inflater, R.layout.schedule_card, parent, false);
        return new ScheduleRVHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleRVHolder holder, int position) {
        Lecture lecture = dataset.get(position);
        holder.setData(lecture);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setDataset(List<Lecture> newDataset) {
        DiffUtil.DiffResult diff = DiffUtil
                .calculateDiff(new ScheduleRVDiffCallback(dataset, newDataset));
        dataset.clear();
        dataset.addAll(newDataset);
        diff.dispatchUpdatesTo(this);
    }

    public void setDayOfWeekFilter(DayOfWeek dayOfWeekFilter) {
        this.dayOfWeekFilter = dayOfWeekFilter;
    }

    public void setGroupFilter(String groupFilter) {
        this.groupFilter = groupFilter;
    }
}
