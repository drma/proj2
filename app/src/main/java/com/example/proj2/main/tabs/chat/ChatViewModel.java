package com.example.proj2.main.tabs.chat;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.proj2.main.tabs.chat.model.Message;
import com.example.proj2.main.tabs.chat.model.MessageFirebaseModel;
import com.example.proj2.main.tabs.chat.model.MessagesLiveData;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ChatViewModel extends AndroidViewModel {

    private DatabaseReference messagesReference;
    private LiveData<List<Message>> messagesLiveData;

    public ChatViewModel(@NonNull Application application) {
        super(application);

        messagesReference = FirebaseDatabase.getInstance().getReference().child("messages");
        messagesLiveData = new MessagesLiveData();
    }

    public LiveData<List<Message>> getMessagesLiveData() {
        return messagesLiveData;
    }

    public void sendMessage(Message message) {
        MessageFirebaseModel fm = new MessageFirebaseModel(
                null,
                message.getFrom().getName(),
                message.getTo().getName(),
                message.getContent(),
                Long.toString(message.getTimestamp().toInstant().getEpochSecond())
        );

        messagesReference.push().setValue(fm);
    }
}
