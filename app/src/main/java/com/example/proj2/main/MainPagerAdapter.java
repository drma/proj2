package com.example.proj2.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.proj2.main.tabs.chat.ChatFragment;
import com.example.proj2.main.tabs.schedule.ScheduleFragment;
import com.example.proj2.main.tabs.WallFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {


    public MainPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    public MainPagerAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }



    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return ScheduleFragment.newInstance();
            case 1: return ChatFragment.newInstance();
            case 2: return WallFragment.newInstance();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return "Schedule";
            case 1: return "Chat";
            case 2: return "Wall";
            default: return null;
        }
    }
}
