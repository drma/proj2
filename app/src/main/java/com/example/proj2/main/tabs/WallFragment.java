package com.example.proj2.main.tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.proj2.R;

public class WallFragment extends Fragment {

    private static final String TAG = "SCHEDULE_FRAGMENT";

    public static WallFragment newInstance() {
        WallFragment instance = new WallFragment();
        Bundle args = new Bundle();
        instance.setArguments(args);
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.schedule, container, false);

        return v;
    }
}
